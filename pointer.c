#include<stdio.h>
void swap(int *a,int *b)
{
int temp;
temp=*a;
*a=*b;
temp=*b;
}
int main()
{
int n1,n2;
printf("Enter the first number\n");
scanf("%d",&n1);
printf("Enter the second number\n");
scanf("%d",&n2);
printf("The value of n1 and n2 before function call is %d%d\n",n1,n2);
swap(&n1,&n2);
printf("The value of n1 and n2 after function call is %d%d\n",n1,n2);
return 0;
}